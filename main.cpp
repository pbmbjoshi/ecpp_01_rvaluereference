// https://www.youtube.com/watch?v=IOkgBrXCtfo
#include <iostream>
using namespace std;

class CArray
{
public:
	CArray()
	{
		cout << "**" << this << ", CArray()" << endl;
	}

	CArray(const int* pArr, const int& size)
	{
		cout << "**" << this << ", CArray(const int* pArr)" << endl;
		if (pArr && size > 0)
		{
			m_ArrSize = size;
			m_pArr = new int[m_ArrSize];
			for (int i = 0; i < m_ArrSize; ++i)
			{
				m_pArr[i] = pArr[i];
			}
		}
	}

	CArray(const CArray& rhs)
	{
		cout << "**" << this << ", rhs : " << &rhs << ", CArray(const CArray& rhs)" << endl;
		*this = rhs;
	}

	
	CArray(CArray&& rhs)
	{
		cout << "**" << this << ", rhs : " << &rhs << ", CArray(const CArray&& rhs)" << endl;
		m_pArr = rhs.m_pArr;
		rhs.m_pArr = nullptr;
	}

	CArray& operator =(const CArray& rhs)
	{
		cout << this << ", rhs : " << &rhs << ", CArray& operator =(const CArray& rhs)" << endl;
		if (this != &rhs)
		{
			if (m_pArr != nullptr)
			{
				cout << "This m_pArr is not null" << endl;
				delete[] m_pArr;
				m_pArr = nullptr;
				m_ArrSize = 0;

			}
			if (rhs.m_pArr)
			{
				cout << "rhs.m_pArr is not null" << endl;
				m_ArrSize = rhs.m_ArrSize;
				m_pArr = new int[m_ArrSize];
				for (int i = 0; i < m_ArrSize; ++i)
				{
					m_pArr[i] = rhs.m_pArr[i];
				}
			}
		}
		return *this;
	}

	CArray& operator =(CArray&& rhs)
	{
		cout << this << ", rhs : " << &rhs << ", CArray& operator =(CArray&& rhs)" << endl;
		if (this != &rhs)
		{
			if (m_pArr != nullptr)
			{
				cout << "This m_pArr is not null" << endl;
				delete[] m_pArr;
				m_pArr = nullptr;

			}
			m_ArrSize = rhs.m_ArrSize;
			m_pArr = rhs.m_pArr;
			rhs.m_pArr = nullptr;
		}
		return *this;
	}

	~CArray()
	{
		if (m_pArr)
		{
			cout << "\t**" << this << ", ~CArray(). m_pArr is not null" << endl;
			delete[] m_pArr;
			m_pArr = nullptr;
		}
		else
		{
			cout << "\t**" << this << ", ~CArray(). m_pArr is null" << endl;
		}
	}

	void display() const
	{
		if (m_pArr)
		{
			cout << this << ", display(). m_pArr is not null" << endl;
			
			for (int i = 0; i < m_ArrSize; ++i)
			{
				cout << m_pArr[i] << "  ";
			}
			cout << endl;
		}
		else
		{
			cout << this << ", display(). m_pArr is null" << endl;
		}
	}

private:
	int* m_pArr = nullptr;
	int m_ArrSize = 0;

};

CArray createArrayObject()
{
	int* pArr = new int[2];
	pArr[0] = 12;
	pArr[1] = 22;
	//pArr[2] = 32;
	//pArr[3] = 42;
	cout << "sizeof(pArr) " << sizeof(pArr) << endl;
	CArray arrObj(pArr,2);
	return arrObj;
}

void CopyArrayObject(CArray& rhs)
{
	int* pArr = new int[2];
	pArr[0] = 12;
	pArr[1] = 22;
	//pArr[2] = 32;
	//pArr[3] = 42;
	cout << "sizeof(pArr) " << sizeof(pArr) << endl;
	CArray arrObj(pArr, 2);
	cout << "Display arrObj " << endl;
	arrObj.display();
	rhs = std::move(arrObj);
	arrObj.display();
}

void displayArray(const CArray& arr)
{
	cout << "Global-displayArray. arr : " << &arr << endl;
	arr.display();
}


int main() {
	cout << "Experiment begin" << endl;
	{
		cout << "Before creating object" << endl;
		CArray a1 = createArrayObject();
		cout << "Before sending object to displayArray" << endl;
		displayArray(a1);
		cout << "Before calling displayArray(createArrayObject())" << endl;
		displayArray(createArrayObject());
		cout << "Create a2 " << endl;
		CArray a2;
		cout << "CopyArrayObject(a2) " << endl;
		CopyArrayObject(a2);
		a2.display();

	}
	cout << "Experiment End" << endl;
	return 0;
}

